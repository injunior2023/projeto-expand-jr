function myFunction() {
    var x = document.getElementById("h-menu-burguer-area");
    var icone = document.getElementById('icon-bars-menu');
    //console.log(icone);
    if (x.style.display == "block") {
      x.style.display = "none";
      icone.classList.remove('fa-close');
      icone.classList.add('fa-bars');
    } else {
      x.style.display = "block";
      icone.classList.add('fa-close');
      icone.classList.remove('fa-bars');
    }
  }