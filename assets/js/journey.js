function selectCircle(item) {
    document.querySelectorAll('.colored_circle').forEach(circle => {
        circle.classList.remove('colored_circle');
    })
    item.classList.add('colored_circle');
} 

function changeDescription(item) {
    let title = item.parentNode.parentNode.firstElementChild

    let description1 = document.querySelector(".journey_description_1")
    let description2 = document.querySelector(".journey_description_2")
    let description3 = document.querySelector(".journey_description_3")
    let description4 = document.querySelector(".journey_description_4")

    description1.style.display = "none"
    description2.style.display = "none"
    description3.style.display = "none"
    description4.style.display = "none"

    if(title.innerHTML == "Primeiro Contato") {
        description1.style.display = "block"
    }
    else if(title.innerHTML == "Diagnostico") {
        description2.style.display = "block"
    }
    else if(title.innerHTML == "Negociação") {
        description3.style.display = "block"
    }
    else if(title.innerHTML == "Inicio do projeto") {
        description4.style.display = "block"
    }
}

document.querySelectorAll('.front_circle').forEach(item => {
    item.addEventListener('click', () => {
        selectCircle(item)
        changeDescription(item)
    })
})