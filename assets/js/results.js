// $(document).ready(function() {
    
// });
var BeenTriggered = false;

$(window).on('scroll', function(e){
    if($(this).scrollTop() >= $('.results').position().top - ($(window).height() * 0.7) && !BeenTriggered){
        BeenTriggered = true;
        $('.cont').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        
    }
});