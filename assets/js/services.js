function openPopUp(card) {
    let popup = document.querySelector(".popup")
    popup.style.display = "block"

    let icon = document.querySelector(".popup_circle")
    icon.innerHTML = card.firstElementChild.innerHTML

    let title = document.querySelector(".popup_title")
    title.innerHTML = card.lastElementChild.innerHTML
}

function closePopUp() {
    let popup = document.querySelector(".popup")
    popup.style.display = "none"
}
function closePopUpNewsletter() {
    let popup = document.querySelector(".popup_newsletter")
    popup.style.display = "none"
}

document.querySelectorAll('.services_circle').forEach(item => {
    item.addEventListener('click', () => {
        openPopUp(item.parentNode);
    })
  })

document.querySelector(".popup_close").addEventListener('click', closePopUp);

document.querySelector(".popup_newsletter_close").addEventListener('click', closePopUpNewsletter);