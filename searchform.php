<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url(home_url('/')) ?>">
    <div>
        <input id="inp_field" type="text" placeholder="Buscar" value="<?php get_search_query(); ?>" name="s" id="s">
    </div>
    <div></div>
</form>