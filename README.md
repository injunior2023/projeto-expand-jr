# Projeto Expand JR

Projeto Expandjr - O repósitorio foi feito pelo grupo La Marguerita Ggantesca, os membros:
1. [Letícia](https://gitlab.com/Leticiamorei)
2. [Juliana](https://gitlab.com/jumouramar)
3. [John Lennon](https://gitlab.com/JohnLennonD)
4. [Racicley](https://gitlab.com/racicley)

O objetivo do projeto foi desenvolver um site em Wordpress para a EJ Expand JR. Os plugins utilizados no projeto, bem como o guia de instalação e configuração do tema, está descrito abaixo.

## Links
-------------
[Manual de Instalação](https://docs.google.com/document/d/18ZD85X28jMkJlNs1UMNrmL09upiPg5_s0rl49W9Tav8/edit#)

[Repositório de Plugins](https://gitlab.com/injunior2023/projeto-expand-jr-plugins)

[Deploy](https://grupo2.injunior.com.br/)