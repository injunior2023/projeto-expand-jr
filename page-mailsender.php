<?php

if(!defined('ABSPATH')){
  exit();
}

if($_SERVER['REQUEST_METHOD'] == "GET"){
    wp_redirect( '/home' );
    exit();
}


$to = esc_html(get_theme_mod( 'set_email', '#' ));
$subject = 'Informações de contato';
$body = '<table class="tg">
<thead>
  <tr>
    <th class="tg-0lax">Nome</th>
    <th class="tg-0lax">'.$_POST["fnome"].'</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0lax">Principal Necessidade:</td>
    <td class="tg-0lax">'.$_POST["fnecessidade"].'</td>
  </tr>
  <tr>
    <td class="tg-0lax">Email:</td>
    <td class="tg-0lax">'.$_POST["femail"].'</td>
  </tr>
  <tr>
    <td class="tg-0lax">Cidade:<br></td>
    <td class="tg-0lax">'.$_POST["fcidade"].'</td>
  </tr>
  <tr>
    <td class="tg-0lax">Telefone:</td>
    <td class="tg-0lax">'.$_POST["ftelefone"].'</td>
  </tr>
  <tr>
  <td class="tg-0lax">Serviços:</td>
  <td class="tg-0lax">'.$_POST["services1"].'<br/>'.$_POST["services2"].'<br/>'.$_POST["services3"].'<br/>'.$_POST["services4"].'<br/>'.$_POST["services5"].'<br/>'.$_POST["services6"].'</td>
</tr>  
<tr>
<td class="tg-0lax">Marketing:</td>
<td class="tg-0lax">'.$_POST["marketing"].'</td>
</tr>

</tbody>
</table>';
$headers = array('Content-Type: text/html; charset=UTF-8');

wp_mail( $to, $subject, $body, $headers );

?>