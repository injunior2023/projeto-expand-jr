<?php
echo get_search_form();
if (is_active_sidebar('sidebar-blog')) : ?>
    <aside class="sidebar">
        <?php dynamic_sidebar('sidebar-blog') ?>
    </aside>
<?php endif;
