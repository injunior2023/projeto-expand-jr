<?php get_header(); ?>
<!-- Arquivo single.php representa página do post individual -->

<div id="primary">
    <main id="main">
        <div class="container">


            <h1>Resultado buscado por: <?php echo get_search_query(); ?></h1>
            <!-- Area do LOOP-->
            <?php


            get_search_form();

            while (have_posts()) :
                the_post();

                get_template_part('parts/content', 'search');

            endwhile;
            the_posts_pagination();
            ?>
        </div>
        <!-- Area do LOOP-->
        <main>
</div>

<?php get_footer(); ?>