<?php
  get_header();  
?>

<section class="banner_blog" style="background: linear-gradient(rgba(0, 0, 0, 0.50), rgba(0, 0, 0, 0.50)),url('<?php echo get_theme_mod( 'set_blog_background_url','') == '' ? IMAGES_DIR.'/bluebackground.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_blog_background_url', '' )),'full')[0] ?>'); background-position: center center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed;">
    <div class="banner_blog_container">
        <h1><?php echo _e('NOSSO BLOG','expand-jr'); ?></h1>
        <p><?php echo _e('Nos conectamos para conectar o mundo','expand-jr'); ?></p>
    </div>
</section>

<section class="blog_content">
    <main class="box-content">
        <?php 
            while (have_posts()) : the_post();
                get_template_part('parts/content', 'search');
                
            endwhile;
           
            
        ?>

    </main>
    <aside>
        <?php 
            echo get_sidebar();
        ?>
    </aside>
   
    
    
</section>
<?php get_template_part('parts/content','pagination'); ?>
<?php
get_footer();
?>