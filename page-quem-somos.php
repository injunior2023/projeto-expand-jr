<?php
    get_header();
?>

<section class="welcome_who" style="background: linear-gradient(rgba(0, 0, 0, 0.50), rgba(0, 0, 0, 0.50)),url('<?php echo get_theme_mod( 'set_quem_somos_background_url','') == '' ? IMAGES_DIR.'/bluebackground.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_quem_somos_background_url', '' )),'full')[0] ?>'); background-position: center center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed;">
    <div class="welcome_who_container">
        <h1><?php echo _e('QUEM SOMOS','expand-jr'); ?></h1>
        <p><?php echo _e('Conheça um pouco da nossa história','expand-jr'); ?></p>
    </div>
</section>

<main class="box_quem_somos">
    <div class="box_quem_somos_container">
        <img src="<?php echo get_theme_mod( 'set_imagem_principal','') == '' ? IMAGES_DIR.'/img.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_imagem_principal', '' )),'full')[0] ?>" alt="">
        <div>
            <h4><?php echo esc_html(get_theme_mod('set_titulo_principal', 'Lorem ipsum dolor')); ?></h4>
            <p><?php echo esc_html(get_theme_mod('set_texto_principal', 'Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque. In dapibus nulla mi, eu elementum quam consequat ac. Nulla mattis, nunc eget viverra imperdiet, justo odio vehicula diam, a scelerisque odio enim id felis. Phasellus tempor libero nec posuere ultrices. Vivamus sit amet fermentum eros. Praesent eget vehicula dolor. Morbi sem magna, scelerisque eu est non, rutrum facilisis massa. Cras ac nulla felis. Nunc eu eleifend nulla. Donec vel ornare diam.')); ?></p>
        </div>
    </div>
</main>

<section class="quem_somos_aside1">
    <div>
        <img src="<?php echo get_theme_mod( 'set_imagem1_aside1','') == '' ? IMAGES_DIR.'/quadrado.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_imagem1_aside1', '' )),'full')[0] ?>" alt="">
        <div>
            <p><?php echo esc_html(get_theme_mod('set_texto1_aside1', 'Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque ed semper commodo purus sit amet facilisis.')); ?></p>
        </div>
    </div>
    <div>
        <img src="<?php echo get_theme_mod( 'set_imagem2_aside1','') == '' ? IMAGES_DIR.'/quadrado.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_imagem2_aside1', '' )),'full')[0] ?>" alt="">
        <div>
            <p><?php echo esc_html(get_theme_mod('set_texto2_aside1', 'Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque ed semper commodo purus sit amet facilisis.')); ?>
            </p>
        </div>
    </div>

</section>


<section class="quem_somos_values">
    <h3><?php echo esc_html(get_theme_mod('set_titulo_valores', 'O que nos move?')); ?></h3>
    <div class="quem_somos_container">
        <div>
            <h3><?php echo esc_html(get_theme_mod('set_titulo_valor1', 'Missão')); ?></h3>
            <img src="<?php echo get_theme_mod( 'set_imagem_valor1','') == '' ? IMAGES_DIR.'/mission.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_imagem_valor1', '' )),'full')[0] ?>" alt="">
            <p><?php echo esc_html(get_theme_mod('set_descricao_valor1', 'Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque')); ?></p>
        </div>
        <div>
            <h3><?php echo esc_html(get_theme_mod('set_titulo_valor2', 'Visão')); ?></h3>
            <img src="<?php echo get_theme_mod( 'set_imagem_valor2','') == '' ? IMAGES_DIR.'/visao.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_imagem_valor2', '' )),'full')[0] ?>" alt="">
            <p><?php echo esc_html(get_theme_mod('set_descricao_valor2', 'Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque')); ?></p>
        </div>
        <div>
            <h3><?php echo esc_html(get_theme_mod('set_titulo_valor3', 'Valores')); ?></h3>
            <img src="<?php echo get_theme_mod( 'set_imagem_valor3','') == '' ? IMAGES_DIR.'/valores.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_imagem_valor3', '' )),'full')[0] ?>" alt="">
            <p><?php echo esc_html(get_theme_mod('set_descricao_valor3', 'Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque')); ?></p>
        </div>
    </div>
</section>


<section class="quem_somos_aside2">
    <div class="quem_somos_aside2_container">
        <div>
            <h4><?php echo esc_html(get_theme_mod('set_titulo_aside2', 'Lorem ipsum dolor')); ?></h4>
            <p><?php echo esc_html(get_theme_mod('set_texto_aside2', 'Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque. In dapibus nulla mi, eu elementum quam consequat ac. Nulla mattis, nunc eget viverra imperdiet, justo odio vehicula diam, a scelerisque odio enim id felis. Phasellus tempor libero nec posuere ultrices. 
            <br><br>
            Vivamus sit amet fermentum eros. Praesent eget vehicula dolor. Morbi sem magna, scelerisque eu est non, rutrum facilisis massa. Cras ac nulla felis. Nunc eu eleifend nulla. Donec vel ornare diam.')); ?></p>
        </div>
        <img src="<?php echo get_theme_mod( 'set_imagem_aside2','') == '' ? IMAGES_DIR.'/img.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_imagem_aside2', '' )),'full')[0] ?>" alt="">
    </div>
</section>


<?php
get_footer();
?>