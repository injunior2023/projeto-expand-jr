</main>
<footer>

<section class="whatsapp">
    <a href="<?php echo esc_html(get_theme_mod( 'set_whatsapp', 'https://wa.me/5521991923790' )); ?>"><img src="<?php echo IMAGES_DIR.'/whatsapp.png'?>" alt=""></a>
</section>

<div class="footer-group">
    <div class="group-top">
    <?php
    //WP_Query para filtrar popular, onde popular é mais comentado
    $args = array(
        'post_status' => 'publish',
        'orderby' => 'comment_count',
        'posts_per_page' => 3
    );
    $query_popular = new WP_Query($args);
    ?>
        <div class="popular-posts">
            <h3 class="popular-title">Postagens Populares</h3>
            <div class="popular-body">
            <?php
                // Start looping over the query results.
                while ( $query_popular->have_posts() ) : $query_popular->the_post();
                    ?>
                    <p> <a href="<?php echo get_permalink(); ?>"><?php echo the_title(); ?></a></p> <?php 
                endwhile;
                // Restore original post data
                wp_reset_postdata();
            ?>
            </div>
        </div>
        <?php
    //WP_Query para filtrar posts mais recentes
    $args = array(
        'order' => 'DESC',
        'post_status' => 'publish',
        'orderby' => 'date',
        'posts_per_page' => 3
    );
    $query_popular = new WP_Query($args);
    ?>
        <div class="recent-posts">
            <h3 class="recent-title">Postagens Recentes</h3>
            <div class="recent-body">
            <?php
                // Start looping over the query results.
                while ( $query_popular->have_posts() ) : $query_popular->the_post();
                    ?>
                    <p> <a href="<?php echo get_permalink(); ?>"><?php echo the_title(); ?></a></p> <?php 
                endwhile;
                // Restore original post data
                wp_reset_postdata();
            ?>
            </div>
        </div>

        <div class="links">
            <h3 class="links-title">Links</h3>
            <div class="links-body">
                <?php
                    wp_nav_menu(array('theme_location' => 'expandJR_Links_footer')); 
                ?>
            </div>
        </div>

    </div>

    <div class="group-bottom">
        <div class="logo-cnpj">
            <h2 class="title-logo">EXPAND JR</h2>
            <p class="cnpj-num">CNPJ: 11.111.111/0000-00</p>
        </div>
        <form class="newsletter-form" action="<?php bloginfo('url'); ?>?na=s" method="POST">
            <label for="email">Cadastre-se em nossa newsletter!</label>
            <input class="inp-mail" type="text" id="email" name="ne" placeholder="Digite seu e-mail">
            <span class="accept-term"> <input type="checkbox" required> Li e aceito as politicas de privacidade e termos de uso </span>
            <button class="btn-cadastro" id="btn-cadastrar" type="submit">Cadastrar</button> 
        </form>
        <div class="address-social">
            <div class="address-custom">
                <p><?php echo esc_html(get_theme_mod( 'set_endereco_footer', 'Please, add some address' )); ?></p>
                <p class="address-fone">Telefone: <?php echo esc_html(get_theme_mod( 'set_telefone_footer', '#' )); ?></p>
            </div>
            <div class="icons-social">
                <a class="icon" href="<?php echo esc_html(get_theme_mod( 'set_links_insta', '#' )); ?>"><i class="fa fa-instagram"></i></a>
                <a class="icon" href="<?php echo esc_html(get_theme_mod( 'set_links_linked', '#' )); ?>"><i class="fa fa-linkedin"></i></a>
                <a class="icon" href="<?php echo 'mailto:' . esc_html(get_theme_mod( 'set_links_email', '#' )); ?>"><i class="fa fa-envelope"></i></a>
            </div>
        </div>
    </div>

</div>
<div class="f-copyright">
    <?php echo esc_html(get_theme_mod( 'set_copyright', 'Copyright ⒸExpand JR 2023' )); ?>
</div>
<?php wp_footer(); ?>
</footer>
</body>
</html>