<?php
date_default_timezone_set('UTC');

if($_SERVER['REQUEST_METHOD'] == "GET"){
    wp_redirect( '/home' );
    exit;
}

if(isset($_POST['news-email'])){
    $hoje = getdate();

    // Create post object
    $post_email_newsletter = 
      array(
        'post_title'    => 'Email Newsletter ' .$hoje['mday'].'/'.$hoje['mon'].'/'.$hoje['year'],
        'post_content'  => 'Inserido em ' .$hoje['mday'].'/'.$hoje['mon'].'/'.$hoje['year']. ' as ' . $hoje['hours'].':'.$hoje['minutes'].':'.$hoje['seconds'],
        'post_status'   => 'private',
        'post_author'   => 1,
        'post_type' => 'newsletter',
        'meta_input'   => array(
          'newsletter_email' => $_POST['news-email']
        )
      );
  
  // Insert the post into the database
  wp_insert_post( $post_email_newsletter );
 
  //$GLOBALS['cgv']['variable-name']  = 'pique';
  wp_redirect( '/home');
  exit;   
}else{

  //$wp->request = 'pique';
  wp_redirect( '/home' );
  exit; 
}

?>