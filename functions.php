<?php

// variaveis
define('ROOT_DIR', get_theme_file_path());
define('STYLES_DIR', get_template_directory_uri() . '/assets/css');
define('INCLUDES_DIR', ROOT_DIR . '/includes');
define('IMAGES_DIR', get_template_directory_uri()  . '/assets/images');
define('SCRIPTS_DIR', get_template_directory_uri() . '/assets/js');

// includes
include_once(INCLUDES_DIR . '/enqueue.php');
//includes do theme customizer
require_once(INCLUDES_DIR . '/costumizer.php');
include_once(INCLUDES_DIR . '/form-contato.php');
include_once(INCLUDES_DIR . '/customizer-quem-somos.php');
include_once(INCLUDES_DIR . '/customizer-blog.php');

// ganchos
add_action('wp_enqueue_scripts', 'expandJR_enqueue_style');
add_action( 'after_setup_theme', 'expandJR_configs_setup' );
add_action( 'widgets_init', 'expandJR_sidebar' );
?>
