<?php
get_header();
?>


<section class="popup_newsletter">
    <div class="smoke"></div>
    <div class="popup_retangle_newsletter">
        <div class="left_div"></div>
        <div class="right_div">
            <button class="popup_newsletter_close">X</button>
            <p>Cadastre-se na newsletter e fique por dentro de todas as novidades e promoções!</p>
            <form action="<?php bloginfo('url'); ?>?na=s" method="POST">
                <label>Seu email:</label><br>
                <input name="ne" type="text"><br><br>
                <input id="register" type="submit" value="Cadastrar">
            </form> 
        </div>
    </div>
</section>

<section class="welcome" style="background: linear-gradient(rgba(0, 0, 0, 0.50), rgba(0, 0, 0, 0.50)),url('<?php echo get_theme_mod( 'set_background_url','') == '' ? IMAGES_DIR.'/bluebackground.png': wp_get_attachment_image_src(esc_html(get_theme_mod( 'set_background_url', '' )),'full')[0] ?>'); background-position: center center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed;">
    <div class="welcome_container" >
        <h1><?php echo _e('Expanda seus horizontes','expand-jr'); ?></h1>
        <p><?php echo _e('através das nossas soluções internacionais de alto impacto.','expand-jr')?></p>
        <button onclick="location.href='<?php bloginfo('url');?>/contato/'" type="button">Fale com um especialista</button>
    </div>
    <div class="arrow_down"></div>
</section>

<section class="services">
        <h2>Nossos Serviços</h2>
        <div class="services_container">
            <div class="services_pf">
                <h2>Para Você</h2>
                <div class="services_cards_col1">
                    <div class="services_card" >
                        <div class="services_circle" style="background: url(<?php echo IMAGES_DIR . "/dupla-cidadania.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
                        <p>Dupla Cidadania Portuguesa</p>
                    </div>
                    <div class="services_card">
                        <div class="services_circle" style="background: url(<?php echo IMAGES_DIR . "/assessoria.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
                        <p>Assessoria para Emissão de Passaporte Brasileiro</p>
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="services_pj">
               <h2>Para Sua Empresa</h2>
               <div class="services_cards_col2">
                    <div class="services_card" >
                        <div class="services_circle" style="background: url(<?php echo IMAGES_DIR . "/analise-buro.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
                        <p>Análise Burocrática</p>
                    </div>
                    <div class="services_card" >
                        <div class="services_circle" style="background: url(<?php echo IMAGES_DIR . "/estudo-mercado.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
                        <p>Estudo e Análise de Mercado</p>
                    </div>
                    <div class="services_card" >
                        <div class="services_circle" style="background: url(<?php echo IMAGES_DIR . "/planejamento-logistico.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
                        <p>Planejamento Logístico</p>
                    </div>
                    <div class="services_card" >
                        <div class="services_circle" style="background: url(<?php echo IMAGES_DIR . "/planejamento-internacional.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
                        <p>Prospecção Internacional</p>
                    </div>
                </div>   
            </div>
        </div>
</section>

<section class="popup">
    <div class="smoke"></div>
    <div class="popup_retangle">
        <button class="popup_close">X</button>
        <div class="popup_circle"></div>
        <h2 class="popup_title">Nome do Serviço</h2>
        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
        <button class="popup_btn" onclick="location.href='<?php bloginfo('url');?>/contato/'" type="button">Diagnóstico Gratuito</button>
    </div>
</section>

<section class="results">
    <h2 class="title_results">Resultados</h2>
    <div class="group_results">
        <div class="results_card">
            <div class="circle_results" style="background: url(<?php echo IMAGES_DIR . "/calendar.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
            <span class="animated_text"> <span class="cont"><?php echo esc_html(get_theme_mod( 'set_resultados_anos', '6' )); ?></span></span>
            <p class="text_results">Anos no mercado</p>
        </div>
        <div class="results_card">
            <div class="circle_results" style="background: url(<?php echo IMAGES_DIR . "/presentation.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
            <span class="animated_text">+ <span class="cont"><?php echo esc_html(get_theme_mod( 'set_resultados_projetos', '70' )); ?></span></span>
            <p class="text_results">Projetos Realizados</p>
        </div>
        <div class="results_card">
            <div class="circle_results" style="background: url(<?php echo IMAGES_DIR . "/graduated.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
            <span class="animated_text">+ <span class="cont"><?php echo esc_html(get_theme_mod( 'set_resultados_estudantes', '100' )); ?></span></span>
            <p class="text_results">Estudantes Impactados</p>
        </div>
        <div class="results_card">
            <div class="circle_results" style="background: url(<?php echo IMAGES_DIR . "/review.png"; ?> ); background-size: 75%;
            background-repeat: no-repeat;
            background-position: center;"></div>
            <span class="animated_text">+ <span class="cont"><?php echo esc_html(get_theme_mod( 'set_resultados_satisfacao', '80' )); ?></span>%</span>
            <p class="text_results">Satisfação dos clientes</p>
        </div>
    </div>
    
</section>

<section class="cases-sucess">
<?php
    $cases = get_theme_mod( 'set_cases', '4' );
    //WP_Query para filtrar post_type , onde popular é mais comentado
    $args = array(
        'post_type' => 'cases-slider',
        'posts_per_page' => $cases
    );
    $query_cases = new WP_Query($args);
    ?>
    

    <h2>Cases de Sucesso</h2>

    <div class="slideshow-container">
    <?php
    
    // Start looping over the query results.
    while ( $query_cases->have_posts() ) : $query_cases->the_post(); ?>
        <div class="mySlides fade">
            <a class="prev" onclick="plusSlides(-1)">❮</a>
            <div class="mySlideContent">
                <p><?php 
                    echo get_post_meta( get_the_ID(), 'cases_slider_description', true ); ?>
                </p>
                <div class="perfil_client">
                    <img src="<?php echo get_post_meta( get_the_ID(), 'cases_slider_img', true ); ?>">
                    <span class="name_client">
                        <?php echo get_post_meta( get_the_ID(), 'cases_slider_name_client', true );  ?>
                    </span>
                </div>
            </div>
            <a class="next" onclick="plusSlides(1)">❯</a>
        </div>
    <?php   
    endwhile; 
    wp_reset_postdata();
    ?>

        
        

    </div>
    <br>

    <div style="text-align:center">
    <?php
    $cases = get_theme_mod( 'set_cases', '4' );
    //WP_Query para filtrar post_type , onde popular é mais comentado
    $args = array(
        'post_type' => 'cases-slider',
        'posts_per_page' => $cases
    );
    $query_cases = new WP_Query($args);
    $i = 0;
    while ( $query_cases->have_posts() ) : $query_cases->the_post();
    $i++;
    ?>
        <span class="dot" onclick="currentSlide(<?php echo $i-1; ?>)"></span> 
    <?php 
        endwhile; ?>

        <!--
        <span class="dot" onclick="currentSlide(1)"></span> 
        <span class="dot" onclick="currentSlide(2)"></span> 
        <span class="dot" onclick="currentSlide(3)"></span>
        --> 
    </div>

</section>

<section class="supporter">
    <h2>Apoiadores</h2>

    <div class="slideshow-container-supporter">
    <?php
    //WP_Query para filtrar post_type 
    //$apoiadores = get_theme_mod( 'set_apoiadores', '4' );
    $args = array(
        'post_type' => 'apoiadores-slider'
        //'posts_per_page' => $apoiadores
    );
    $query_cases = new WP_Query($args);

    ?>
    <a class="prev" onclick="add(1)">❮</a>
    <div class="slides-container-supporter">
    <?php
        // Start looping over the query results.
        while ( $query_cases->have_posts() ) : $query_cases->the_post(); ?>
            <div class="slides-supporter">
                <img src="<?php echo get_post_meta( get_the_ID(), 'apoiadores_slider_img', true ); ?>">
            </div>
        <?php endwhile; ?>
    </div>
        
        <a class="next" onclick="add(-1)">❯</a>

    </div>
    

    <div style="text-align:center">

    <?php
    //$apoiadores = get_theme_mod( 'set_apoiadores', '4' );
    //WP_Query para filtrar post_type , onde popular é mais comentado
    $args = array(
        'post_type' => 'apoiadores-slider',
        //'posts_per_page' => $apoiadores
    );
    $query_cases = new WP_Query($args);
    $i = 0;
    while ( $query_cases->have_posts() ) : $query_cases->the_post();
    $i++;
    ?>
        <span onclick="showSlidesSingle(<?php echo $i-1; ?>)"></span> 
    <?php endwhile; ?>
    </div>
</section>

<?php
get_footer();
?>