<div class="box-pagination">
    <?php
    the_posts_pagination(array(
        'mid_size'  => 2,
        'prev_text' => __( '❮', 'expand-jr' ),
        'next_text' => __( '❯', 'expand-jr' ),
    ));
    ?>
</div>