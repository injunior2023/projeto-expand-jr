<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <?php if ('post' == get_post_type()) : ?>
        <div class="post_content">
            <div class="image-content">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="meta-info">
                <h2 class="blog_title"> <?php the_title(); ?></h2>
                <span class="light_cat"><?php the_category(' '); ?></span>
                <?php the_excerpt(); ?>
                <a class="btn_readmore" href="<?php the_permalink(); ?>">Ler Mais</a>
            </div>
        </div>
    <?php endif; ?>     
    
</article>