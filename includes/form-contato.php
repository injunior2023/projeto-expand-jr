<?php


function expandjr_form_contato($wp_customize){
// Painel
$wp_customize->add_panel( 'pagina_contato', array(
    'title' => __( 'Página Contato' ),
    'description' => 'Configurações da página contato', // Include html tags such as <p>.
    'priority' => 160, // Mixed with top-level-section hierarchy.
  ) );

// Seção Background /************************************************/
$wp_customize->add_section(
    'sec_background', array(
        'title'			=> 'Configurações de Background',
        'description'	=> 'Seção de Background',
        'panel' => 'pagina_contato'
    )
);

    // Campo 1 - Background Image
    $wp_customize->add_setting(
        'set_contato_background_url', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_contato_background_url', array(
        'label' => __( 'Background', 'expand-jr' ),
        'description'		=> 'Adicione uma imagem',
        'section' => 'sec_background',
        'mime_type' => 'image',
    ) ) );
  
// Seção
$wp_customize->add_section(
    'sec_contato', array(
        'title'      => 'Configuração de contato',
        'description'  => 'Seção contato',
        'panel' => 'pagina_contato'
    )
);

    // Campo 1 - Copyright Text Box
    $wp_customize->add_setting(
        'set_telefone', array(
            'type'          => 'theme_mod',
            'default'        => '',
            'sanitize_callback'    => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_telefone', array(
            'label'        => 'Telefone',
            'description'    => 'Por favor, adicione o telefone',
            'section'      => 'sec_contato',
            'type'        => 'text'
        )
    );

    $wp_customize->add_setting(
        'set_email', array(
            'type'          => 'theme_mod',
            'default'        => '',
            'sanitize_callback'    => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_email', array(
            'label'        => 'Email',
            'description'    => 'Por favor, adicione o email',
            'section'      => 'sec_contato',
            'type'        => 'text'
        )
    );

    $wp_customize->add_setting(
        'set_endereco', array(
            'type'          => 'theme_mod',
            'default'        => '',
            'sanitize_callback'    => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_endereco', array(
            'label'        => 'Endereço',
            'description'    => 'Por favor, adicione o endereço',
            'section'      => 'sec_contato',
            'type'        => 'text'
        )
    );

// Seção Jornada do Cliente
$wp_customize->add_section(
    'jornada', array(
        'title'      => 'Jornada do Cliente',
        'description'  => 'Seção jornada do cliente',
        'panel' => 'pagina_contato'
    )
);

    $wp_customize->add_setting(
        'set_descricao_primeiro_contato', array(
            'type'          => 'theme_mod',
            'default'        => '',
            'sanitize_callback'    => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_descricao_primeiro_contato', array(
            'label'        => 'Descrição primeiro contato',
            'description'    => 'Por favor, adicione uma descrição para o primeiro contato',
            'section'      => 'jornada',
            'type'        => 'text'
        )
    );

    $wp_customize->add_setting(
        'set_descricao_diagnostico', array(
            'type'          => 'theme_mod',
            'default'        => '',
            'sanitize_callback'    => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_descricao_diagnostico', array(
            'label'        => 'Descrição diagnostico',
            'description'    => 'Por favor, adicione uma descrição para o diagnostico',
            'section'      => 'jornada',
            'type'        => 'text'
        )
    );

    $wp_customize->add_setting(
        'set_descricao_negociacao', array(
            'type'          => 'theme_mod',
            'default'        => '',
            'sanitize_callback'    => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_descricao_negociacao', array(
            'label'        => 'Descrição negociação',
            'description'    => 'Por favor, adicione uma descrição para a negociação',
            'section'      => 'jornada',
            'type'        => 'text'
        )
    );

    $wp_customize->add_setting(
        'set_descricao_inicio_projeto', array(
            'type'          => 'theme_mod',
            'default'        => '',
            'sanitize_callback'    => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_descricao_inicio_projeto', array(
            'label'        => 'Descrição inicio do projeto',
            'description'    => 'Por favor, adicione uma descrição para o inicio do projeto',
            'section'      => 'jornada',
            'type'        => 'text'
        )
    );

}


add_action('customize_register', 'expandjr_form_contato');


?>