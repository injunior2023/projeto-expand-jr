<?php
//CSS e JS
function expandJR_enqueue_style(){
    wp_enqueue_style('expandJR-reset', STYLES_DIR . '/reset.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-style', STYLES_DIR . '/style.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-general-style', STYLES_DIR . '/general.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-home-style', STYLES_DIR . '/home.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-quemsomos-style', STYLES_DIR . '/quemsomos.css', [], '1.0.0', false);
    wp_enqueue_style('style-contato', STYLES_DIR . '/contato.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-blog-style', STYLES_DIR . '/blog.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-home-slider', STYLES_DIR . '/slider.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-home-slider-sup', STYLES_DIR . '/sliderSupporter.css', [], '1.0.0', false);
    wp_enqueue_style('expandJR-burguer-css', STYLES_DIR . '/burguer.css', [], '1.0.0', false);
    
    //google-fonts
    wp_enqueue_style('google-fonts','https://fonts.googleapis.com/css2?family=Barlow:wght@500;600;700&family=Inter:wght@400;500;600;700&family=Roboto:ital,wght@0,400;0,500;0,900;1,300&display=swap', [],'1.0.0', false);

    //Font-Awesome
    wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', [],'1.0.0', false );

    //JQuery
    wp_enqueue_script( 'jquery-variable', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js', ['jquery'],'1.0.0', true );

    //script
    wp_enqueue_script('expandJR-services-js', SCRIPTS_DIR . '/services.js', [], '1.0.0', true);
    wp_enqueue_script('expandJR-results-js', SCRIPTS_DIR . '/results.js', [], '1.0.0', true);
    wp_enqueue_script('expandJR-journey-js', SCRIPTS_DIR . '/journey.js', [], '1.0.0', true);
    wp_enqueue_script('expandJR-slider-js', SCRIPTS_DIR . '/slider.js', [], '1.0.0', true);
    wp_enqueue_script('expandJR-sliderSup-js', SCRIPTS_DIR . '/sliderSupporter.js', [], '1.0.0', true);
    wp_enqueue_script('expandJR-slider-apoiadores-js', SCRIPTS_DIR . '/case_slider.js', [], '1.0.0', true);
    wp_enqueue_script('expandJR-multi-select-js', SCRIPTS_DIR . '/multi_selected.js', [], '1.0.0', true);
    wp_enqueue_script('expandJR-burguer-js', SCRIPTS_DIR . '/burguer.js', [], '1.0.0', true);
}

//Configurações Header
function expandJR_configs_setup() {
    //Custom logo
	$defaults = array(
		'height'               => 50,
		'width'                => 200,
		'flex-height'          => true,
		'flex-width'           => true
	);
	add_theme_support( 'custom-logo', $defaults );

   //Registrando o menu
   register_nav_menus(
    //Array contendo chave(id menu) e valor sendo o nome amigável do menu
    //Obs.: Mais de um menu pode ser adicionado
    array(
        'expandJR_main_menu' => 'Main Menu'
    )
    );

    register_nav_menus( 
        array(
            'expandJR_Links_footer' => 'Footer Links'
        )
    );
    /* Support thumbnails posts */
    add_theme_support('post-thumbnails');


}

//Sidebar
function expandJR_sidebar(){
        //sidebar, não é necessariamente lateral
        register_sidebar(
            array(
                'name' => 'Blog Sidebar',
                'id' => 'sidebar-blog',
                'description' => 'Esta é a sidebar do Blog. Você pode adicionar suas widgets aqui.',
                'before_widget' => '<div class="widget-wrapper">',
                'after_widget' => '</div>',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>'
            )
        );
}

?>
