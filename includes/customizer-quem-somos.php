<?php

function expandJR_customizer_quem_somos_configs($wp_customize){

// Painel
    $wp_customize->add_panel( 'pagina_quem_somos', array(
        'title' => __( 'Página Quem Somos' ),
        'description' => 'Configurações da página quem somos',
        'priority' => 160,
      ) );

    // BACKGROUND /************************************************/
    $wp_customize->add_section(
        'sec_quem_somos_background', array(
            'title'			=> 'Configurações de Background',
            'description'	=> 'Seção de Background',
            'panel' => 'pagina_quem_somos'
        )
    );

        // Campo 1 - Background Image
        $wp_customize->add_setting(
            'set_quem_somos_background_url', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );

        $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_quem_somos_background_url', array(
            'label' => __( 'Background', 'expand-jr' ),
            'description'		=> 'Adicione uma imagem',
            'section' => 'sec_quem_somos_background',
            'mime_type' => 'image',
        ) ) );
      
    // MAIN /************************************************/
    $wp_customize->add_section(
        'sec_principal', array(
            'title'			=> 'Configurações da seção principal',
            'description'	=> 'Seção principal',
            'panel' => 'pagina_quem_somos'
        )
    );
    
    // Campo 1 - Imagem
    $wp_customize->add_setting(
        'set_imagem_principal', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_imagem_principal', array(
        'label' => __( 'Imagem principal', 'expand-jr' ),
        'description'		=> 'Por favor, adicione a imagem da seção principal',
        'section' => 'sec_principal',
        'mime_type' => 'image',
    )));

    // Campo 2 - Título Main
    $wp_customize->add_setting(
        'set_titulo_principal', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_titulo_principal', array(
            'label'				=> 'Título da seção principal',
            'description'		=> 'Adicione o texto do título da seção principal',
            'section'			=> 'sec_principal',
            'type'				=> 'text'
        )
    );

    // Campo 3 - Texto Main
    $wp_customize->add_setting(
        'set_texto_principal', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_texto_principal', array(
            'label'				=> 'Texto da seção principal',
            'description'		=> 'Adicione o texto da seção principal',
            'section'			=> 'sec_principal',
            'type'				=> 'text'
        )
    );


    // SEÇÃO ASIDE 1 /************************************************/
    $wp_customize->add_section(
        'sec_aside1', array(
            'title'			=> 'Configurações do Aside 1',
            'description'	=> 'Seção Aside 1',
            'panel' => 'pagina_quem_somos'
        )
    );

    // Campo 1 - Imagem 1 aside 1
    $wp_customize->add_setting(
        'set_imagem1_aside1', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_imagem1_aside1', array(
        'label' => __( 'Imagem 1 Aside 1', 'expand-jr' ),
        'description'		=> 'Por favor, adicione a imagem 1 do Aside 1',
        'section' => 'sec_aside1',
        'mime_type' => 'image',
    )));

    // Campo 2 - Texto 1 aside 1
    $wp_customize->add_setting(
        'set_texto1_aside1', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_texto1_aside1', array(
            'label'				=> 'Texto 1 do Aside 1',
            'description'		=> 'Adicione o texto 1 do Aside 1',
            'section'			=> 'sec_aside1',
            'type'				=> 'text'
        )
    );

    // Campo 3 - Imagem 2 aside 1
    $wp_customize->add_setting(
        'set_imagem2_aside1', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_imagem2_aside1', array(
        'label' => __( 'Imagem 2 Aside 1', 'expand-jr' ),
        'description'		=> 'Por favor, adicione a imagem 2 do Aside 1',
        'section' => 'sec_aside1',
        'mime_type' => 'image',
    )));

    // Campo 4 - Texto 1 aside 1
    $wp_customize->add_setting(
        'set_texto2_aside1', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_texto2_aside1', array(
            'label'				=> 'Texto 2 do Aside 1',
            'description'		=> 'Adicione o texto 2 do Aside 1',
            'section'			=> 'sec_aside1',
            'type'				=> 'text'
        )
    );
      

    // SEÇÃO VALORES /************************************************/
    $wp_customize->add_section(
        'sec_valores', array(
            'title'			=> 'Configurações de Valores',
            'description'	=> 'Seção Valores',
            'panel' => 'pagina_quem_somos'
        )
    );

        // Campo 1 - Título
        $wp_customize->add_setting(
            'set_titulo_valores', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );

        $wp_customize->add_control(
            'set_titulo_valores', array(
                'label'				=> 'Título',
                'description'		=> 'Por favor, adicione o texto do título',
                'section'			=> 'sec_valores',
                'type'				=> 'text'
            )
        );

        // Campo 2 - Título Valor 1
        $wp_customize->add_setting(
            'set_titulo_valor1', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );
    
        $wp_customize->add_control(
            'set_titulo_valor1', array(
                'label'				=> 'Nome valor 1',
                'description'		=> 'Por favor, adicione o nome do título do valor 1',
                'section'			=> 'sec_valores',
                'type'				=> 'text'
            )
        );

        // Campo 3 - Imagem Valor 1
        $wp_customize->add_setting(
            'set_imagem_valor1', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );

        $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_imagem_valor1', array(
            'label' => __( 'Imagem valor 1', 'expand-jr' ),
            'description'		=> 'Por favor, adicione a imagem do valor 1',
            'section' => 'sec_valores',
            'mime_type' => 'image',
        )));

        // Campo 4 - Descrição Valor 1
        $wp_customize->add_setting(
            'set_descricao_valor1', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );
    
        $wp_customize->add_control(
            'set_descricao_valor1', array(
                'label'				=> 'Descrição valor 1',
                'description'		=> 'Por favor, adicione a descrição do valor 1',
                'section'			=> 'sec_valores',
                'type'				=> 'text'
            )
        );

        // Campo 5 - Título Valor 2
        $wp_customize->add_setting(
            'set_titulo_valor2', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );
    
        $wp_customize->add_control(
            'set_titulo_valor2', array(
                'label'				=> 'Nome valor 2',
                'description'		=> 'Por favor, adicione o nome do título do valor 2',
                'section'			=> 'sec_valores',
                'type'				=> 'text'
            )
        );
        
        // Campo 6 - Imagem Valor 2
        $wp_customize->add_setting(
            'set_imagem_valor2', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );

        $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_imagem_valor2', array(
            'label' => __( 'Imagem valor 2', 'expand-jr' ),
            'description'		=> 'Por favor, adicione a imagem do valor 2',
            'section' => 'sec_valores',
            'mime_type' => 'image',
        )));

        // Campo 7 - Descrição Valor 2
        $wp_customize->add_setting(
            'set_descricao_valor2', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );
    
        $wp_customize->add_control(
            'set_descricao_valor2', array(
                'label'				=> 'Descrição valor 2',
                'description'		=> 'Por favor, adicione a descrição do valor 2',
                'section'			=> 'sec_valores',
                'type'				=> 'text'
            )
        );

        // Campo 8 - Título Valor 3
        $wp_customize->add_setting(
            'set_titulo_valor3', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );
    
        $wp_customize->add_control(
            'set_titulo_valor3', array(
                'label'				=> 'Nome valor 3',
                'description'		=> 'Por favor, adicione o nome do título do valor 3',
                'section'			=> 'sec_valores',
                'type'				=> 'text'
            )
        );

        // Campo 9 - Imagem Valor 3
        $wp_customize->add_setting(
            'set_imagem_valor3', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );

        $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_imagem_valor3', array(
            'label' => __( 'Imagem valor 3', 'expand-jr' ),
            'description'		=> 'Por favor, adicione a imagem do valor 3',
            'section' => 'sec_valores',
            'mime_type' => 'image',
        )));

        // Campo 10 - Descrição Valor 3
        $wp_customize->add_setting(
            'set_descricao_valor3', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );
    
        $wp_customize->add_control(
            'set_descricao_valor3', array(
                'label'				=> 'Descrição valor 3',
                'description'		=> 'Por favor, adicione a descrição do valor 3',
                'section'			=> 'sec_valores',
                'type'				=> 'text'
            )
        );

    // SEÇÃO ASIDE 2 /************************************************/
    $wp_customize->add_section(
        'sec_aside2', array(
            'title'			=> 'Configurações do Aside 2',
            'description'	=> 'Seção Aside 2',
            'panel' => 'pagina_quem_somos'
        )
    );

    // Campo 1 - Título Aside 2
    $wp_customize->add_setting(
        'set_titulo_aside2', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_titulo_aside2', array(
            'label'				=> 'Título Aside 2',
            'description'		=> 'Adicione o texto do título do Aside 2',
            'section'			=> 'sec_aside2',
            'type'				=> 'text'
        )
    );

    // Campo 2 - Texto Aside 2
    $wp_customize->add_setting(
        'set_texto_aside2', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_texto_aside2', array(
            'label'				=> 'Texto Aside 2',
            'description'		=> 'Adicione o texto do Aside 2',
            'section'			=> 'sec_aside2',
            'type'				=> 'text'
        )
    );

    // Campo 3 - Imagem Aside 2
    $wp_customize->add_setting(
        'set_imagem_aside2', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_imagem_aside2', array(
        'label' => __( 'Imagem Aside 2', 'expand-jr' ),
        'description'		=> 'Adicione a imagem do aside 2',
        'section' => 'sec_aside2',
        'mime_type' => 'image',
    )));
}

add_action( 'customize_register', 'expandJR_customizer_quem_somos_configs');

?>