
<?php
function expandJR_blog_customizer_configs($wp_customize){
// Painel
$wp_customize->add_panel( 'pagina_blog', array(
    'title' => __( 'Página Blog' ),
    'description' => 'Configurações da página blog',
    'priority' => 160,
  ) );

// BACKGROUND /************************************************/
$wp_customize->add_section(
    'sec_blog_background', array(
        'title'			=> 'Configurações de Background',
        'description'	=> 'Seção de Background',
        'panel' => 'pagina_blog'
    )
);

    // Campo 1 - Background Image
    $wp_customize->add_setting(
        'set_blog_background_url', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_blog_background_url', array(
        'label' => __( 'Background', 'expand-jr' ),
        'description'		=> 'Adicione uma imagem',
        'section' => 'sec_blog_background',
        'mime_type' => 'image',
    ) ) );
}
add_action('customize_register', 'expandJR_blog_customizer_configs');
?>