<?php
function expandJR_customizer_configs($wp_customize){
//Painel
$wp_customize->add_panel( 'pagina_inicial', array(
    'title' => __( 'Página Inicial' ),
    'description' => 'Configurações da página inicial', // Include html tags such as <p>.
    'priority' => 160, // Mixed with top-level-section hierarchy.
  ) );

// Seção
$wp_customize->add_section(
    'sec_copyright', array(
        'title'			=> 'Configurações de copyright',
        'description'	=> 'Seção copyright',
        'panel' => 'pagina_inicial'
    )
);

    // Campo 1 - Copyright Text Box
    $wp_customize->add_setting(
        'set_copyright', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_copyright', array(
            'label'				=> 'Copyright',
            'description'		=> 'Por favor, adicione um copyright aqui',
            'section'			=> 'sec_copyright',
            'type'				=> 'text'
        )
    );

    /************************************************/
    // Seção Links
    $wp_customize->add_section(
    'sec_links', array(
        'title'			=> 'Configurações de links',
        'description'	=> 'Seção links',
        'panel' => 'pagina_inicial'
        )
    );

    // Campo 1 - Instagram Url
    $wp_customize->add_setting(
        'set_links_insta', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    // Campo 2 -  LinkedIn Url
    $wp_customize->add_setting(
    'set_links_linked', array(
        'type'					=> 'theme_mod',
        'default'				=> '',
        'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    // Campo 2 -  Email Url
    $wp_customize->add_setting(
    'set_links_email', array(
        'type'					=> 'theme_mod',
        'default'				=> '',
        'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_links_insta', array(
            'label'				=> 'Instagram',
            'description'		=> 'Por favor, insira o link do instagram aqui',
            'section'			=> 'sec_links',
            'type'				=> 'text'
        )
    );

    $wp_customize->add_control(
        'set_links_linked', array(
            'label'				=> 'LinkedIn',
            'description'		=> 'Por favor, insira o link do linkedin aqui',
            'section'			=> 'sec_links',
            'type'				=> 'text'
        )
    );

    $wp_customize->add_control(
        'set_links_email', array(
            'label'				=> 'Email',
            'description'		=> 'Por favor, adicione o email aqui',
            'section'			=> 'sec_links',
            'type'				=> 'text'
        )
    );

    /************************************************/
    // Seção Endereço / Telefone
    $wp_customize->add_section(
    'sec_endereco_telefone', array(
        'title'			=> 'Configurações de endereço/telefone',
        'description'	=> 'Seção endereço',
        'panel' => 'pagina_inicial'
        )
    );

     // Campo 1 - Endereço
     $wp_customize->add_setting(
        'set_endereco_footer', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

      // Campo 2 - Telefone
      $wp_customize->add_setting(
        'set_telefone_footer', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_endereco_footer', array(
            'label'				=> 'Endereço',
            'description'		=> 'Por favor, insira seu endereço completo aqui',
            'section'			=> 'sec_endereco_telefone',
            'type'				=> 'text'
        )
    );

    $wp_customize->add_control(
        'set_telefone_footer', array(
            'label'				=> 'Telefone',
            'description'		=> 'Por favor, insira o telefone aqui',
            'section'			=> 'sec_endereco_telefone',
            'type'				=> 'text'
        )
    );
    /************************************************/
    //Seção Top Header
    $wp_customize->add_section(
        'sec_header', array(
            'title'			=> 'Configurações de cabeçalho',
            'description'	=> 'Seção cabeçalho',
            'panel' => 'pagina_inicial'
        )
    );

     // Campo 1 - Background Image
     $wp_customize->add_setting(
        'set_background_url', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );


    $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'set_background_url', array(
        'label' => __( 'Background', 'expand-jr' ),
        'description'		=> 'Please, add a image',
        'section' => 'sec_header',
        'mime_type' => 'image',
      ) ) );

    // $wp_customize->add_control(
    //     'set_copyright', array(
    //         'label'				=> 'Copyright',
    //         'description'		=> 'Please, add your copyright information here',
    //         'section'			=> 'sec_copyright',
    //         'type'				=> 'text'
    //     )
    // );
        /***********************************************************/
        // Seção
$wp_customize->add_section(
    'sec_resultados', array(
        'title'			=> 'Configurações de resultados',
        'description'	=> 'Seção resultados',
        'panel' => 'pagina_inicial'
    )
);

    // Campo 1 - Anos no mercado
    $wp_customize->add_setting(
        'set_resultados_anos', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_resultados_anos', array(
            'label'				=> 'Anos no mercado',
            'description'		=> 'Por favor, o número de anos no mercado.',
            'section'			=> 'sec_resultados',
            'type'				=> 'number',
            //'input_attrs' => array(
                //'class' => 'my-custom-class-for-js',
                //'style' => 'border: 1px solid #900',
                //'placeholder' => __( '6' ),
             // ),
              //'active_callback' => 'is_front_page',
        )
    );

     // Campo 2 - projetos
     $wp_customize->add_setting(
        'set_resultados_projetos', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_resultados_projetos', array(
            'label'				=> 'Projetos',
            'description'		=> 'Por favor, o número de projetos realizados.',
            'section'			=> 'sec_resultados',
            'type'				=> 'number'
        )
    );

     // Campo 3 - Estudantes impactados 
     $wp_customize->add_setting(
        'set_resultados_estudantes', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_resultados_estudantes', array(
            'label'				=> 'Estudantes Impactados',
            'description'		=> 'Por favor, o número de estudantes impactados.',
            'section'			=> 'sec_resultados',
            'type'				=> 'number'
        )
    );

     // Campo 4 - Satisfação dos estudantes
     $wp_customize->add_setting(
        'set_resultados_satisfacao', array(
            'type'					=> 'theme_mod',
            'default'				=> '',
            'sanitize_callback'		=> 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_resultados_satisfacao', array(
            'label'				=> 'Satisfação dos Clientes',
            'description'		=> 'Por favor, o percentual de clientes satisfeitos.',
            'section'			=> 'sec_resultados',
            'type'				=> 'number'
        )
    );

    // Seção Whatsapp
    $wp_customize->add_section(
        'sec_whatsapp', array(
            'title'			=> 'Configurações de whatsapp',
            'description'	=> 'Seção whatsapp',
            'panel' => 'pagina_inicial'
            )
        );
    
        $wp_customize->add_setting(
            'set_whatsapp', array(
                'type'					=> 'theme_mod',
                'default'				=> '',
                'sanitize_callback'		=> 'sanitize_text_field'
            )
        );
        $wp_customize->add_control(
            'set_whatsapp', array(
                'label'				=> 'Whatsapp',
                'description'		=> 'Por favor, insira o link do whatsapp aqui',
                'section'			=> 'sec_whatsapp',
                'type'				=> 'text'
            )
        );

/*********************************************************/


 
}
add_action( 'customize_register', 'expandJR_customizer_configs');
?>