<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?> <?php the_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body>
<header>
    <div class='header-group'>
        <div class="h-logo">
            <?php
            if (has_custom_logo()) {
                echo get_custom_logo();
            } else {
            ?>
                <a href="<?php echo home_url('/'); ?>">
                    <span><?php bloginfo('name'); ?></span>
                </a>
            <?php
            }
            ?>
        </div>
        <div class="h-menu">
            <?php wp_nav_menu(array('theme_location' => 'expandJR_main_menu', 'depth' => 2)); ?>
        </div>

        <div class="h-translate">
            <div id="weglot_here"></div>
        </div>
        <div class="h-menu-burguer-bars">    
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars" id="icon-bars-menu"></i>
            </a>
        </div>
    </div>
</header>
<main>
<section class="h-menu-burguer-area" id="h-menu-burguer-area">
    <div class="h-menu-burguer" id="">
        <?php wp_nav_menu(array('theme_location' => 'expandJR_main_menu', 'depth' => 2)); ?>
        
    </div>
</section>  